import { Cidade } from './../shared/modelo/cidade';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { BaseFormComponent } from '../shared/base-form/base-form.component';
import { FormularioModel } from '../template-form/formulario.model';
import { FormValidations } from './../shared/form-validations';
import { Estadobr } from './../shared/modelo/estadobr';
import { ConsultaCepService } from './../shared/services/consulta-cep.service';
import { DropdownService } from './../shared/services/dropdown.service';
import { VerificaEmailService } from './services/verifica-email.service';

@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.css']
})
export class DataFormComponent extends BaseFormComponent implements OnInit {

  model = new FormularioModel();
  estados: Estadobr[];
  cidades: Cidade[];
  cargos: any[];
  tecnologias: any[];
  newsletterOp: any[];

  framework = ['Angular', 'React', 'Vue', 'Sencha'];
  verificaEmailService: any;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private dropdownService: DropdownService,
    private cepService: ConsultaCepService,
    private VerificaEmailService: VerificaEmailService,
  ) {
    super();

    this.formulario = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.email], [this.validarEmail.bind(this)]],
      confirmarEmail: [null, [FormValidations.equalsTo('email123')]],

      cep: ['', [Validators.required, FormValidations.cepValidator]],
      numero: ['', [Validators.required]],
      complemento: ['', [Validators.maxLength(25)]],
      rua: ['', [Validators.required]],
      bairro: ['', [Validators.required]],
      cidade: ['', [Validators.required]],
      estado: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
      cargo: [null],
      tecnologias: [null],
      newsletter: [null],
      termos: [null, Validators.pattern('true')],
      frameworks: this.buildFrameworks()
    });

  }

  buildFrameworks() {

    const values = this.framework.map(v => new FormControl(false));
    return this.formBuilder.array(values, [Validators.required, FormValidations.requiredMinCheckbox(1)]);
  }

  ngOnInit(): void {

    this.dropdownService.getEstados()
      .subscribe((resp: Estadobr[]) => this.estados = resp);

    this.cargos = this.dropdownService.getCargos();
    this.tecnologias = this.dropdownService.getTecnologias();
    this.newsletterOp = this.dropdownService.getNewsletter();

    this.formulario.get('estado').valueChanges
    .pipe(
      tap(estado => console.log('Novo estado: ', estado)),
      map(estado => this.estados.filter(e => e.sigla === estado)),
      map(estados => estados && estados.length > 0 ? estados[0].id : null),
      switchMap((estadoId: number) => this.dropdownService.getCidades(estadoId)),
      tap(console.log)
    )
    .subscribe(cidades => this.cidades = cidades);

  }

  submit() {
    console.log(this.formulario.valid);

    if (this.formulario.invalid) {
      this.formulario.markAllAsTouched();
      return;
    }
    let valueSubmit = Object.assign({}, this.formulario.value);

    valueSubmit = Object.assign(valueSubmit, {
      frameworks: valueSubmit.frameworks
        .map((v, i) => v ? this.framework[i] : null)
        .filter(v => v !== null),
    })

    if (this.formulario) {
      this.http.post('https://httpbin.org/post',
        JSON.stringify(valueSubmit))
        .subscribe(dados => console.log(dados));
      {
        this.formulario.reset();
      }
      (error: any) => alert('erro');
    }
  }

  verificaValidacoesForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(campo => {
      console.log(campo);
      const controle = formGroup.get(campo);
      controle.markAsDirty();
      if (controle instanceof FormGroup) {
        this.verificaValidacoesForm(controle);
      }
    })
  }

  consultaCEP() {

    const cep = this.formulario.get('cep').value;
    this.getCEP(cep).subscribe(data => {
      this.formulario.get('rua').patchValue(data.logradouro);
      this.formulario.get('complemento').patchValue(data.complemento);
      this.formulario.get('bairro').patchValue(data.bairro);
      this.formulario.get('cidade').patchValue(data.localidade);
      this.formulario.get('estado').patchValue(data.uf);

    });

  }

  getCEP(cep: string): Observable<any> {
    cep = cep?.replace(/\D/g, '');

    if (cep != "") {

      var validacep = /^[0-9]{8}$/;

      if (validacep.test(cep)) {

        return this.http.get(`//viacep.com.br/ws/${cep}/json`);
      }
    }
  }

  setarCargo() {
    const cargo = { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl' };
    this.formulario.get('cargo').setValue(cargo);
  }

  compararCargos(obj1, obj2) {
    return obj1 && obj2 ? (obj1.nome === obj2.nome && obj1.nivel === obj2.nivel) : obj1 === obj2;
  }

  setarTecnologias() {
    this.formulario.get('tecnologias').setValue(['java', 'javascript', 'php']);
  }

  validarEmail(formControl: FormControl) {
    return this.VerificaEmailService.verificarEmail(formControl.value)
      .pipe(map(emailExiste => emailExiste ? { emailInvalido: true } : null));
  }

}
