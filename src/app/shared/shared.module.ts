import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CampoControlErroComponent } from './/campo-control-erro/campo-control-erro.component';
import { CardCrudComponent } from './card-crud/card-crud.component';
import { FormDebugComponent } from './form-debug/form-debug.component';
import { DropdownService } from './services/dropdown.service';
import { ErrorMsgComponent } from './error-msg/error-msg.component';
import { InputFieldComponent } from './input-field/input-field.component';
import { BaseFormComponent } from './base-form/base-form.component';


@NgModule({
    imports: [
        FormsModule,
        CommonModule,
    ],
    declarations: [
        CardCrudComponent,
        FormDebugComponent,
        CampoControlErroComponent,
        ErrorMsgComponent,
        InputFieldComponent,
    ],
    exports: [
        CardCrudComponent,
        CampoControlErroComponent,
        FormDebugComponent,
        ErrorMsgComponent,
        InputFieldComponent,
    ],
    providers: [ DropdownService ],

})
export class SharedModule { }