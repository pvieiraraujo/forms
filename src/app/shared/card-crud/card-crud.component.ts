import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-card-crud',
  templateUrl: './card-crud.component.html',
  styleUrls: ['./card-crud.component.css']
})
export class CardCrudComponent implements OnInit {

  constructor() { }

  @Input() title;
  @Input() form;
  @Input() isDebug: boolean = true;

  ngOnInit() {

  }
}