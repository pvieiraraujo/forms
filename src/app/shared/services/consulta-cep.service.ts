import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsultaCepService {
  formulario: any;

  constructor(private http: HttpClient) { }

  consultaCEP(cep: string) {

    cep = this.formulario.get('cep').value;
    this.getCEP(cep).subscribe(data => {
      this.formulario.get('rua').patchValue(data.logradouro);
      this.formulario.get('complemento').patchValue(data.complemento);
      this.formulario.get('bairro').patchValue(data.bairro);
      this.formulario.get('cidade').patchValue(data.localidade);
      this.formulario.get('estado').patchValue(data.uf);
    });

  }

  getCEP(cep: string): Observable<any> {
    cep = cep?.replace(/\D/g, '');

    if (cep != "") {

      var validacep = /^[0-9]{8}$/;

      if (validacep.test(cep)) {

        return this.http.get(`//viacep.com.br/ws/${cep}/json`);
      }
    }
  }
}
