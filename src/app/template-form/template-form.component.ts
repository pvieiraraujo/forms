import { ConsultaCepService } from './../shared/services/consulta-cep.service';
import { FormularioModel } from './formulario.model';
import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
// import 'rxjs/add/operator/map';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {

  model = new FormularioModel();



  usuario: any = {
    nome: null,
    email: null
  }

  constructor(
    private http: HttpClient,
    private cepService: ConsultaCepService,
    ) { }

  onSubmit(form) {
    console.log(form);

    // console.log(this.usuario);

    this.http.post('https://httpbin.org/post', JSON.stringify(form.value));
    // .map(res => res)
    // .subscribe( dados => console.log(dados));
  }

  ngOnInit(): void {

  }

  verificaValidTouched(campo) {
  //   return !campo.valid && campo.touched
   }

  aplicaCssErro(campo) {
    return {
      'has-error': this.verificaValidTouched(campo)
    }
  }

  consultaCEP(cep, form) {
    cep = cep.replace(/\D/g, '');

    if (cep != "") {

      var validacep = /^[0-9]{8}$/;

      if (validacep.test(cep)) {

        this.http.get(`//viacep.com.br/ws/${cep}/json`)
          .subscribe(
            dados => this.populaDadosForm(dados)
            );

      }

    }
  }

  populaDadosForm(dados){
    
    this.model.rua = dados.logradouro;
    this.model.complemento = dados.complemento;
    this.model.bairro = dados.bairro;
    this.model.cidade = dados.localidade;
    this.model.estado = dados.uf;
    
    // formulario.setValue({
    //   nome: formulario.value.nome,
    //   email: formulario.value.email,
    //   endereço: {
    //     cep: dados.cep,
    //     numero: '',
    //     complemento:dados.complemento,
    //     // rua: dados.logradouro,
    //     bairro: dados.bairro,
    //     cidade: dados.cidade,
    //     estado: dados.estado,
    //   }
    // });
  }

  
}



